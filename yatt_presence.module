<?php

/**
 * @file
 * YATT Presence
 *
 * @author
 * Stefan Auditor <stefan.auditor@erdfisch.de>
 *
 * @todo
 *  - Better default views
 *  - Delete form for an entry
 *  - Edit form for an entry
 *  - Display confirmation form with posibility to edit the valued
 *    for time entries that are longer than the deafult work day on
 *    next login / page request.
 */

/**
 * Implementation of hook_views_api().
 */
function yatt_presence_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'yatt_presence') . '/includes',
  );
}

/**
 * Implement hook_menu().
 */
function yatt_presence_menu() {
  $items = array();
  $items['yatt-presence'] = array(
    'title' => 'Presence capture',
    'description' => 'Tweak display and other settings',
    'page callback' => 'yatt_presence_capture_page',
    'access arguments' => array('access yatt presence'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/settings/yatt-presence'] = array(
    'title' => 'YATT presence settings',
    'description' => 'Tweak display and other settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('yatt_presence_admin_settings'),
    'access arguments' => array('administer yatt presence'),
    'type' => MENU_NORMAL_ITEM,
    'file' => '/includes/yatt_presence.admin.inc',
  );
  $items['yatt-presence/edit/%yatt_presence'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('yatt_presence_edit_form', 2),
    'access callback' => 'yatt_presence_access',
    'access arguments' => array('update', 2),
    'type' => MENU_CALLBACK,
  );
//  $items['yatt-presence/%yatt_presence/edit'] = array(
//    'title' => 'Edit',
//    'page callback' => 'yatt_presence_edit_page',
//    'page arguments' => array(),
//    'access arguments' => array('access yatt presence'),
//    'type' => MENU_NORMAL_ITEM,
//  );
  $items['yatt-presence/%yatt_presence/delete'] = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('yatt_presence_delete_confirm', 1),
    'access callback' => 'yatt_presence_access',
    'access arguments' => array('delete', 1),
    'type' => MENU_CALLBACK,
  );
  $items['yatt-presence/who-is-present'] = array(
    'title' => 'Who Is Present',
    'page callback' => 'yatt_presence_wip_page',
    'access arguments' => array('access yatt presence'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implement hook_perm().
 */
function yatt_presence_perm() {
  return array(
    'administer yatt presence',
    'access yatt presence',
    'edit own yatt presence',
    'edit any yatt presence',
    'delete own yatt presence',
    'delete any yatt presence',
  );

}

function yatt_presence_access($op, $yp, $account = NULL) {
  global $user;

  if (!$yp || !in_array($op, array('view', 'update', 'delete', 'create'), TRUE)) {
    return FALSE;
  }

  if ($op != 'create') {
    $yp = (object)$yp;
  }

  if (empty($account)) {
    $account = $user;
  }

  if (user_access('administer yatt presence', $account)) {
    return TRUE;
  }

  switch ($op) {
    case 'delete':
      if (user_access('delete own yatt presence') && $yp->uid == $account->uid && $account->uid != 0) {
        return TRUE;
      }
      if (user_access('delete any yatt presence')) {
        return TRUE;
      }
      break;
    case 'update':
      if (user_access('edit own yatt presence') && $yp->uid == $account->uid && $account->uid != 0) {
        return TRUE;
      }
      if (user_access('edit any yatt presence')) {
        return TRUE;
      }
      break;
  }
  return FALSE;
}

/**
 * Implementation of hook_load().
 */
function yatt_presence_load($param = array()) {
  $arguments = array();
  if (is_numeric($param)) {
    $cond = 'yp.ypid = %d';
    $arguments[] = $param;
  }
  elseif (is_array($param)) {
    foreach ($param as $key => $value) {
      $cond[] = 'yp.' . db_escape_table($key) . " = '%s'";
      $arguments[] = $value;
    }
    $cond = implode(' AND ', $cond);
  }
  else {
    return FALSE;
  }
  return db_fetch_object(db_query("SELECT yp.* FROM {yatt_presence} yp WHERE " . $cond, $arguments));
}

/**
 *
 */
function yatt_presence_delete($ypid) {
  $yp = yatt_presence_load($ypid);

  if (yatt_presence_access('delete', $yp)) {
    db_query("DELETE FROM {yatt_presence} WHERE ypid = %d", $yp->ypid);
    drupal_set_message(t('Presence has been deleted.'));
  }
}

/**
 * Implement hook_user().
 */
function yatt_presence_user($op, &$edit, &$account, $category = NULL) {
  global $user;
  switch ($op) {
    case 'view':
      // Access control
      if (!user_access('access yatt presence')
          OR !variable_get('yatt_presence_display_profile', FALSE)) {
        return;
      }

      $account->content['summary']['yatt'] = array(
        '#type' => 'item',
        '#title' => t('Presence toggle'),
        '#value' => drupal_get_form('yatt_presence_toggle_form', $account->yatt_presence),
        '#attributes' => array('class' => 'yatt-presence-toggle'),
        '#access' => $account->uid == $user->uid || user_access('administer yatt presence'),
      );
      break;
    case 'load':
      $account->yatt_presence = yatt_presence_get_status($account->uid);
      break;
    case 'delete':
      yatt_presence_delete_by_user($account->uid);
      break;
  }
}

/**
 * Implement hook_nodeapi().
 */
function yatt_presence_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  global $user;
  switch ($op) {
    case 'view':
      // Access control
      if (!user_access('access yatt presence')
          OR !variable_get('yatt_presence_display_node', FALSE)) {
        return;
      }

      // Teaser view
      if ($a3) {
        return;
      }

//      $node->content['yatt_presence_toggle'] = array(
//        '#value' => theme('yatt_presence_toggle_block', $user),
//        '#weight' => 10,
//      );
      break;
  }
}

/**
 * Implement hook_block().
 */
function yatt_presence_block($op = 'list', $delta = 0, $edit = array()) {
  if ($op == 'list') {
    $blocks[0] = array(
      'info' => t('Presence toggle'),
      'cache' => BLOCK_CACHE_PER_PAGE,
    );
    return $blocks;
  }
  elseif ($op == 'view') {
    switch ($delta) {
      case 0:
        // Access control
        if (!user_access('access yatt presence')) {
          return;
        }

        global $user;
        $status = yatt_presence_get_status($user->uid);
        if ($status && !$status->departure) {
          $title = '<span class="yatt-presence-is-present">&laquo; ' . yatt_format_time_diff(time() - $status->arrival, ':') . ' &raquo;</span>';
        }
        else {
          $title = t('zzZz');
        }
        $block = array(
          'subject' => $title, //t('Presence toggle'),
          'content' => theme('yatt_presence_toggle_block', NULL),
        );
        break;
    }
    return $block;
  }
}

/**
 * Presence capture form
 */
function yatt_presence_capture_form($form_state) {
  global $user;
  $form = array();
  $form['date'] = array(
    '#type' => 'date',
    '#title' => t('Date'),
  );
  $form['time'] = array(
    '#type' => 'textfield',
    '#title' => t('Time'),
    '#description' => t('Type 1:30 or 1.5 for 1 hour and 30 minutes. Or type 8:00 - 13:30 for a time frame'),
    '#required' => TRUE,
  );
  $locations = explode(',', variable_get('yatt_presence_locations', 'Office,Home Office'));
  $form['location'] = array(
    '#type' => 'select',
    '#title' => t('Location'),
    '#options' => drupal_map_assoc($locations),
  );
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $user->uid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save time'),
  );
  return $form;
}

/**
 * Implementation of hook_form_validate().
 */
function yatt_presence_capture_form_validate($form, &$form_state) {
  $time_frame = yatt_presence_parse_time($form_state['values']['time'], $form_state['values']['date']);
  if ($time_frame === FALSE) {
    form_set_error('time', t('Please enter a valid time'));
  }
  else {
    $form_state['values']['arrival']    = $time_frame['start'];
    $form_state['values']['departure']  = $time_frame['stop'];
    $form_state['values']['created']    = time();
  }
}

/**
 * Implementation of hook_form_submit().
 */
function yatt_presence_capture_form_submit($form, &$form_state) {
  $arrival   = $form_state['values']['arrival'];
  $departure = $form_state['values']['departure'];
  $duration  = $departure - $arrival;
  drupal_write_record('yatt_presence', $form_state['values']);

  drupal_set_message(t('The time frame from !arrival to !departure has been captured. This results in !duration duration.', array('!arrival' => format_date($arrival), '!departure' => format_date($departure), '!duration' => format_interval($duration))));
}

/**
 * Form for arrival/departure
 */
function yatt_presence_toggle_form($form_state, $status = array()) {
  $form = array();
  $form['info'] = array(
    '#value' => theme('yatt_presence_status', $status),
  );
  if (!$status OR $status->departure) {
    $locations = explode(',', variable_get('yatt_presence_locations', 'Office,Home Office'));
    $form['location'] = array(
      '#type' => 'select',
      '#title' => t('Current location'),
      '#options' => drupal_map_assoc($locations),
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => (!$status OR $status->departure) ? t('Presence toggle arrive') : t('Presence toggle depart'),
  );
  return $form;
}

/**
 * Presence toggle form; Validate handler
 */
function yatt_presence_toggle_form_validate($form, &$form_state) {
  global $user;
  $form_state['values']['uid']       = $form_state['values']['uid']       ? $form_state['values']['uid']       : $user->uid;
  $form_state['values']['arrival']   = $form_state['values']['arrival']   ? $form_state['values']['arrival']   : time();
  $form_state['values']['created']   = $form_state['values']['created']   ? $form_state['values']['created']   : time();

  $clicked = $form_state['clicked_button']['#value'];
  if ($clicked == t('Presence toggle arrive')) {
    $form_state['values']['departure'] = $form_state['values']['departure'] ? $form_state['values']['departure'] : 0;
  }
  else {
    $form_state['values']['departure'] = $form_state['values']['departure'] ? $form_state['values']['departure'] : time();
  }
}

/**
 * Presence toggle form; Submit handler
 */
function yatt_presence_toggle_form_submit($form, &$form_state) {
  global $user;
  $clicked = $form_state['clicked_button']['#value'];

  // Only one timer may run per user
  yatt_presence_stop($user->uid);

  if ($clicked == t('Presence toggle arrive')) {
    drupal_set_message(t('You are now marked as present at !location', array('!location' => t($form_state['values']['location']))));
    yatt_presence_start($user->uid, $form_state['values']);
  }
  else {
    drupal_set_message(t('You are now marked as absent.'));
  }
}

/**
 * Presence edit form
 */
//function yatt_presence_edit_form($form_state, $ypid, $ref) {
function yatt_presence_edit_form($form_state, $yp) {
  $ref = referer_uri();
  $form = array();

  $form['arrival'] = array(
    '#type' => 'fieldset',
    '#title' => t('Arrival'),
  );
  $form['arrival']['arrival_date'] = array(
    '#type' => 'date',
    '#title' => t('Arrival date'),
    '#default_value' => array(
        'year' => date("Y", $yp->arrival),
        'month' => date("n", $yp->arrival),
        'day' => date("j", $yp->arrival),
    ),
  );
  $form['arrival']['arrival_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Arrival time'),
    '#description' => t('Format: HH:MM'),
    '#default_value' => date("H:i", $yp->arrival),
  );
  $form['departure'] = array(
    '#type' => 'fieldset',
    '#title' => t('Departure'),
  );

  $departure = $yp->departure == 0 ? time() : $yp->departure;

  $form['departure']['departure_date'] = array(
    '#type' => 'date',
    '#title' => t('Departure date'),
    '#default_value' => array(
        'year' => date("Y", $departure),
        'month' => date("n", $departure),
        'day' => date("j", $departure),
    ),
  );
  $form['departure']['departure_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Departure time'),
    '#description' => t('Format: HH:MM'),
    '#default_value' => date("H:i", $departure),
  );

  $locations = explode(',', variable_get('yatt_presence_locations', 'Office,Home Office'));
  $form['location'] = array(
    '#type' => 'select',
    '#title' => t('Location'),
    '#options' => drupal_map_assoc($locations),
    '#default_value' => $yp->location,
  );
  $form['ypid'] = array(
    '#type' => 'value',
    '#value' => $yp->ypid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );
//  $form['cancel'] = array(
//    '#type' => 'markup',
//    '#value' => l(t('Cancel'), $ref),
//  );
  $form['ref'] = array(
    '#type' => 'value',
    '#value' => $ref,
  );

  return $form;
}

/**
 * Presence edit form; Validate handler
 */
function yatt_presence_edit_form_validate($form, &$form_state) {
  // TODO

}

/**
 * Presence edit form; Submit handler
 */
function yatt_presence_edit_form_submit($form, &$form_state) {

  if ($form_state['values']['op'] == $form_state['values']['submit']) {
    $arrival = explode(':', $form_state['values']['arrival_time']);
    $arrival_date = $form_state['values']['arrival_date'];
    $ts_arrival = mktime($arrival[0], $arrival[1], 0, $arrival_date['month'], $arrival_date['day'], $arrival_date['year']);
    $departure = explode(':', $form_state['values']['departure_time']);
    $departure_date = $form_state['values']['departure_date'];
    $ts_departure = mktime($departure[0], $departure[1], 0, $departure_date['month'], $departure_date['day'], $departure_date['year']);

    db_query("UPDATE {yatt_presence} SET arrival=%d, departure=%d, location='%s' WHERE ypid=%d", $ts_arrival, $ts_departure, $form_state['values']['location'], $form_state['values']['ypid']);
    drupal_set_message(t('Changes saved.'));
    $ref = $form_state['values']['ref'];
    $form_state['redirect'] = 'yatt-presence';
  }
  elseif ($form_state['values']['op'] == $form_state['values']['delete']) {
    drupal_goto('yatt-presence/' . $form_state['values']['ypid'] . '/delete');
  }
  elseif ($form_state['values']['op'] == $form_state['values']['cancel']) {
    $form_state['redirect'] = 'yatt-presence';
  }
}

/**
 * Menu callback -- ask for confirmation of yatt presence deletion
 */
function yatt_presence_delete_confirm(&$form_state, $yp) {
   $form['ypid'] = array(
    '#type' => 'value',
    '#value' => $yp->ypid,
  );
  $account = user_load($yp->uid);
  return confirm_form($form,
    t('Are you sure you want to delete the yatt presence %user: %from - %to (%time) ?',
        array(
          '%user' => $account->name,
          '%from' => date('d.m.Y H:i', $yp->arrival),
          '%to' => date('d.m.Y H:i', $yp->departure),
          '%time' => yatt_format_time_diff($yp->departure - $yp->arrival, ':'),
        )
    ),
    isset($_GET['destination']) ? $_GET['destination'] : 'yatt-presence',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute yatt presence deletion
 */
function yatt_presence_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    yatt_presence_delete($form_state['values']['ypid']);
  }

  $form_state['redirect'] = 'yatt-presence';
}

/**
 * Who Is Present
 */
function yatt_presence_wip_page() {
  $header = array(t('Name'), t('Location'), t('Present since'), t('Time'), t('Stop'));
  $rows = array();
  $result = db_query("SELECT y.*, u.name as username, u.uid FROM {yatt_presence} y INNER JOIN {users} u ON y.uid = u.uid WHERE departure = %d", 0);
  while ($row = db_fetch_object($result)) {
    $rows[] = array(
        $row->username,
        $row->location,
        date('H:i', $row->arrival),
        yatt_format_time_diff(time() - $row->arrival, ':'),
        drupal_get_form('yatt_presence_stop_timer_form_' . $row->uid, $row->uid)
    );
  }
  $output = theme('table', $header, $rows);
  if (!$rows) {
    $output .= t('There is nobody present at the moment.');
  }
  return $output;
}

function yatt_presence_forms($form_id, $args) {
  $forms = array();
  if (strpos($form_id, 'yatt_presence_stop_timer_form_') === 0) {
    $forms[$form_id] = array(
      'callback' => 'yatt_presence_stop_timer_form'
    );
  }
  return $forms;
}

function yatt_presence_stop_timer_form($form_state, $uid) {
  $form = array();
  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $uid
  );
  $form['#submit'] = array(
    'yatt_presence_stop_timer_form_submit',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Stop'),
  );
  return $form;
}
function yatt_presence_stop_timer_form_submit($form, &$form_state) {
  yatt_presence_stop($form_state['values']['uid']);
}

/**
 * Start tracking a users's time
 */
function yatt_presence_start($uid, $form_values = NULL) {
  return drupal_write_record('yatt_presence', $form_values);
}

/**
 * Stop tracking a users's time
 */
function yatt_presence_stop($uid, $time = NULL) {
  $time = is_numeric($time) ? $time : time();
  return db_query("UPDATE {yatt_presence} SET departure = %d WHERE uid = %d AND departure = %d", $time, $uid, 0);
}

/**
 * Get current user's tracking status
 */
function yatt_presence_get_status($uid = NULL) {
  if (!$uid) {
    global $user;
    $uid = $user->uid;
  }
  $result = db_query("SELECT * FROM {yatt_presence} WHERE uid = %d ORDER BY ypid DESC", $uid);
  return db_fetch_object($result);
}

/**
 * Delete a user's presence time
 */
function yatt_presence_delete_by_user($uid) {
  return db_query("DELETE FROM {yatt_presence} WHERE uid = %d", $uid);
}

/**
 * Parse various time formats
 */
function yatt_presence_parse_time($time, $date) {
  // 10 - 13:00 || 8:00-12:15 || 14:00 16:00
  if (preg_match('/((0|1)\d|2[0-3]|\d)(:[0-5]\d)?[\s-]+((0|1)\d|2[0-3]|\d)(:[0-5]\d)?/', $time, $matches) && $matches[0] == $time) {
    $time_array = preg_split('/(\s-\s|-|\s)/', $time);
    $from = explode(':', $time_array[0]);
    $to = explode(':', $time_array[1]);
    $time_frame = array(
      'start' => mktime($from[0], $from[1], 0, $date['month'], $date['day'], $date['year']),
      'stop' => (($to[0] < $from[0]))
        ? (mktime($to[0], $to[1], 0, $date['month'], $date['day'] + 1, $date['year']))
        : (mktime($to[0], $to[1], 0, $date['month'], $date['day'], $date['year'])),
    );
    return $time_frame;
  }
  // 0,5 oder 0.30 oder 4
  if (preg_match('/(\d{1,2})((,|\.)(0{1,2}|25|5(0{0,1})|75))?/', $time, $matches) && $matches[0] == $time) {
    $time_array = preg_split('/(,|\.)/', $time);
    if (count($time_array) <= 2) {
      $time_array[] = 0;
    }
    $min = ($time_array[1] < 10) ? ($time_array[1] / 10) : ($time_array[1] / 100);
    $delta_minutes = floor(60 * $min);
    $time_frame = array(
      'start' => mktime(date('H') - $time_array[0], date('i') - $delta_minutes, date('s'), $date['month'], $date['day'], $date['year']),
      'stop' => mktime(date('H'), date('i'), date('s'), $date['month'], $date['day'], $date['year']),
    );
    return $time_frame;
  }
  // 00:30
  if (preg_match('/\d{1,2}:[0-5]\d/', $time, $matches) && $matches[0] == $time) {
    $time_array = explode(':', $time);
    $time_frame = array(
      'start' => mktime(date('H') - $time_array[0], date('i') - $time_array[1], date('s'), $date['month'], $date['day'], $date['year']),
      'stop' => mktime(date('H'), date('i'), date('s'), $date['month'], $date['day'], $date['year']),
    );
    return $time_frame;
  }
  return FALSE;
}

/**
 * Presence capture page
 */
function yatt_presence_capture_page() {

  if (arg(1)) {
    global $user;
    $state = yatt_presence_get_status($user->uid);
    $is_present = $state->departure ? FALSE : TRUE;
    $location = $state->location;

    if (arg(1) == 'start' AND $is_present) {
      drupal_set_message(t('You are already marked as present at !location', array('!location' => $state->location)));
    }
    elseif (arg(1) == 'stop' AND !$is_present) {
      drupal_set_message(t('You are already marked as absent.'));
    }
    elseif ((arg(1) == 'start' OR arg(1) == 'toggle') AND !$is_present) {
      $locations = explode(',', variable_get('yatt_presence_locations', 'Office,Home Office'));
      $obj = array(
        'uid' => $user->uid,
        'arrival' => time(),
        'created' => time(),
        'location' => arg(2) ? arg(2) : $locations[0],
      );
      yatt_presence_start($user->uid, $obj);
      drupal_set_message(t('You are now marked as present at !location', array('!location' => $obj['location'])));
    }
    elseif ((arg(1) == 'stop' OR arg(1) == 'toggle') AND $is_present){
      yatt_presence_stop($user->uid);
      drupal_set_message(t('You are now marked as absent.'));
    }
  }

  $output = drupal_get_form('yatt_presence_capture_form');
  $output .= '<h2>' . t('Captured presence times') . '</h2>';
  $output .= views_embed_view('yatt_presence', 'default');
  return $output;
}

/**
 * Implement hook_theme().
 */
function yatt_presence_theme($existing, $type, $theme, $path) {
  return array(
    'yatt_presence_toggle_block' => array(
      'arguments' => array('account' => NULL),
    ),
    'yatt_presence_status' => array(
      'arguments' => array('status' => array()),
    ),
  );
}

/**
 * Themeable presence toggle block
 */
function theme_yatt_presence_toggle_block($account) {
  $status = yatt_presence_get_status($account->uid);
  if ($status && !$status->departure) {
    drupal_add_css(drupal_get_path('module', 'yatt_presence') . '/css/yatt_presence-toggle.css');
  }
  $output = drupal_get_form('yatt_presence_toggle_form', $status);
  $output .= '<ul class="links">';
//  $output .= '<div>' . t('For more options use the !url form.', array('!url' => l('Presence capture', 'yatt-presence'))) . '</div>';
  $output .= '<li>' . l('Presence capture', 'yatt-presence') .'</li>';
  if (user_access('access yatt presence')) {
    $output .= '<li>' . l('Who is present', 'yatt-presence/who-is-present') . '</li>';
  }
  $output .= '</ul>';
  return $output;
}

/**
 * Themeable status message
 */
function theme_yatt_presence_status($status) {
  $output = '<div class="yatt-presence-status">';
  $output .= '<div class="presence-status' . ($status->departure ? ' status-not-present' : ' status-present') . '">';
  if (!$status OR $status->departure) {
    $output .= t('You are absent.');
  }
  else {
    $output .= t('You are present at <span class="presence-location">!location</span> since <span class="presence-time-ago">!time_ago</span>', array('!location' => $status->location, '!time_ago' => format_interval(time() - $status->arrival)));
  }
  $output .= '</div>';
  $output .= '</div>';
  return $output;
}

/**
 *
 * Implementation of hook_xmlrpc().
 */
function yatt_presence_xmlrpc() {
  return array(
    array(
      'yatt.presence.test',
      'yatt_presence_xmlrpc_test',
      array('array', 'string', 'string'),
      t('test function'),
    ),
    array(
      'yatt.presence.login',
      'yatt_presence_xmlrpc_login',
      array('int', 'string', 'string'),
      t('login in'),
    ),
    array(
      'yatt.presence.logout',
      'yatt_presence_xmlrpc_logout',
      array('int'),
      t('login in'),
    ),
    array(
      'yatt.presence.get.status',
      'yatt_presence_xmlrpc_get_status',
      array('array', 'string', 'string'),
      t('get the yatt presence status'),
    ),
    array(
      'yatt.presence.start',
      'yatt_presence_xmlrpc_start',
      array('array', 'string', 'string'),
      t('start the yatt presence')
    ),
    array(
      'yatt.presence.stop',
      'yatt_presence_xmlrpc_stop',
      array('array', 'string', 'string', 'int'),
      t('stop the yatt presence')
    ),
  );
}

function yatt_presence_xmlrpc_login($username, $password) {
  global $user;
  if ($user->uid) {
    return (int) $user->uid;
  }
  $user = user_authenticate(array('name' => $username, 'pass' => $password));
  if ($user->uid) {
    session_start();
    return (int) $user->uid;
  }
  session_destroy();
  return (int) 0;
}

function yatt_presence_xmlrpc_logout() {
  global $user;
  if (!$user->uid) {
    return (int) 0;
  }
  session_destroy();
  module_invoke_all('user', 'logout', NULL, $user);
  $user = drupal_anonymous_user();
  return (int) 1;
}

/**
 *
 * @return array
 */
function yatt_presence_xmlrpc_get_status($username, $password) {

  $uid = yatt_presence_xmlrpc_login($username, $password);
  if (!$uid) {
    return yatt_presence_xmlrpc_error_message("Login failed!\nUsername or password wrong.");
  }
  if (!user_access('access yatt presence')) {
    return yatt_presence_xmlrpc_error_message('Permission denied!');
  }
  $result = array(
    'status' => TRUE,
    'result' => yatt_presence_get_status($uid),
  );
  yatt_presence_xmlrpc_logout();
  return $result;
}

function yatt_presence_xmlrpc_start($username, $password) {
  $uid = yatt_presence_xmlrpc_login($username, $password);
  if (!$uid) {
    return yatt_presence_xmlrpc_error_message("Login failed!\nUsername or password wrong.");
  }
  if (!user_access('access yatt presence')) {
    return yatt_presence_xmlrpc_error_message('Permission denied!');
  }
  $locations = explode(',', variable_get('yatt_presence_locations', 'Office,Home Office'));
  $data = array(
    'uid' => $uid,
    'arrival' => time(),
    'departure' => 0,
    'created' => time(),
    'location' => $locations[0],
  );
  $result = yatt_presence_start($uid, $data);
  if ($result === FALSE) {
    return yatt_presence_xmlrpc_error_message('Start presence failed');
  }
  return array(
    'status' => TRUE,
    'result' => array('arrival' => $data['arrival']),
  );
}

function yatt_presence_xmlrpc_stop($username, $password, $time) {
  $uid = yatt_presence_xmlrpc_login($username, $password);
  if (!$uid) {
    return yatt_presence_xmlrpc_error_message("Login failed!\nUsername or password wrong.");
  }
  if (!user_access('access yatt presence')) {
    return yatt_presence_xmlrpc_error_message('Permission denied!');
  }
  $time = $time == 0 ? NULL : $time;
  yatt_presence_stop($uid, $time);
  return array(
    'status' => TRUE,
    'result' => '',
  );
}

function yatt_presence_xmlrpc_error_message($msg) {
  return array(
    'status' => FALSE,
    'error_message' => $msg,
  );
}

function yatt_presence_xmlrpc_test($username, $password) {
  $uid = yatt_presence_xmlrpc_login($username, $password);
  if (!$uid) {
    return yatt_presence_xmlrpc_error_message("Login failed!\nUsername or password wrong.");
  }
  if (!user_access('access yatt presence')) {
    return yatt_presence_xmlrpc_error_message("Permission denied!");
  }
  return array(
    'status' => TRUE,
    'result' => 'Test passed'
  );
}










