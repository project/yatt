$(document).ready(function() {
    $("#edit-yatt-reminder").click(function() {
        $('#edit-yatt-time').val(yatt_reminder_time);
        for (i = yatt_reminder_from; i <= yatt_reminder_to; i++) {
            $('.yatt-selectable-nojs input:eq('+ i +')').attr('checked', true);
            $('.selectables .ui-selectee:eq('+ i +')').addClass('ui-selected');
        }
    });
});