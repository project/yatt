(function($) {
  $.fn.addTimer = function(settings) {
    var config = {
      'start': 0,
      'count': 'countup', // countup, countdown
      'refresh': 1000,
      'class': 'timer'
    };

    if (settings) $.extend(config, settings);
    var init = 0;

    this.each(function() {
      formatTime = function(counter) {
        var days    = Math.floor(counter / 86400);
        var hours   = Math.floor((counter - (days * 86400 )) / 3600);
        var minutes = Math.floor((counter - (days * 86400 ) - (hours * 3600 )) / 60);
        var secs    = Math.floor((counter - (days * 86400 ) - (hours * 3600 ) - (minutes * 60)));
        var x       = counter + "(" + days + " : " + hours + " : " + minutes + " : " + secs + ")";
        return x;
      }

      count = function() {
        $('.'+config.class).each(function() {
          var counter = $(this).attr('value');
          if ($(this).hasClass('timer-countup')) {
            counter++;
          }
          else {
            counter--;
          }
          $(this).attr('value', counter).html(formatTime(counter));
        });
        
        window.setTimeout("count()", config.refresh);
      }

      config.start = parseFloat(config.start);
      $(this).prepend('<span value="' + config.start + '" class="' + config.class + ' ' + config.class + '-' + config.count + '">' + formatTime(config.start) + '</span>');
      if (init == 0) {
        window.setTimeout("count()", config.refresh);
        init = 1;
      }
    });

    return this;
  };
})(jQuery);
$('#header').addTimer({'count': 'countup', 'start': 25});
$('#block-user-1').addTimer({'count': 'countdown', 'start': 25});

function display_ct(this) {
  var days    = Math.floor(counter / 86400);
  var hours   = Math.floor((counter - (days * 86400 )) / 3600);
  var minutes = Math.floor((counter - (days * 86400 ) - (hours * 3600 )) / 60);
  var secs    = Math.floor((counter - (days * 86400 ) - (hours * 3600 ) - (minutes * 60)));
  var x       = counter + "(" + days + " : " + hours + " : " + minutes + " : " + secs + ")";

  document.getElementById('ct').innerHTML = x;
  window.start= window.start + 1;
  tt = display_c(window.start);
}

display_c(86501);
</script>

<span id='ct' style="background-color: #FFFF00"></span>