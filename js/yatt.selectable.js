//TODO: check for check selector and select accordingly
Drupal.behaviors.yattSimpleForm = function () {
  update = function () {
	  $(".selectables .ui-selected").each(function() {
	    var index = $(".selectables li").index(this);
	    items.push(index+1);
	  });
	  $('.yatt-selectable-nojs input').attr('checked', false);
	  for (index in items) {
	    $('.yatt-selectable-nojs input:eq('+(items[index] - 1)+')').attr('checked', true);
	  }
	  items = new Array();
  };
  init = function () {
    $(".yatt-selectable-nojs input[checked]").each(function() {
      var index = $(".yatt-selectable-nojs input").index(this);
      items.push(index+1);
    });
    $('.selectables').removeClass('ui-selected');
	for (index in items) {
	  $('.selectables .ui-selectee:eq('+(items[index] - 1)+')').addClass('ui-selected');
	}
    items = new Array();
  }

  $percent = (1 / $('.selectables dl').size() * 100);
  $('.selectables dl').css('width', $percent+'%');

  $('.yatt-selectable-js').show();
  $('.yatt-selectable-nojs').hide();
  var items = new Array();
  $(".selectables").selectable({
    filter: 'li',
    stop: function() {
	  update();
	}
  });
  $(".selectables li").click(function() {
    $(this).toggleClass('ui-selected');
    update();
  });
  init();
}