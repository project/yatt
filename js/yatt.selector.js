Drupal.behaviors.yattSelector = function () {
  if (yatt_selected == 'selectables') {
      $('.yatt-textfield-comment-form').hide();
  }
  else {
      $('.yatt-selectables-comment-form').hide();
  }
  $('.yatt-selector-wrapper input').hide();
  $('#edit-yatt-selector-selectables-wrapper label').addClass('active');

  $('#edit-yatt-selector-textfield').
    change(function() {
      $('#edit-yatt-selector-selectables-wrapper label').toggleClass('active');
      $('#edit-yatt-selector-textfield-wrapper label').toggleClass('active');
      $('.yatt-selectables-comment-form').hide().removeClass('active');
      $('.yatt-textfield-comment-form').show().addClass('active');
  });
  $('#edit-yatt-selector-selectables').
    addClass('active').
    change(function() {
      $('#edit-yatt-selector-selectables-wrapper label').toggleClass('active');
      $('#edit-yatt-selector-textfield-wrapper label').toggleClass('active');
      $('.yatt-selectables-comment-form').show().addClass('active');
      $('.yatt-textfield-comment-form').hide().removeClass('active');
  });
}