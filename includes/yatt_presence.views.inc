<?php

/**
 * @file
 * YATT Presence - Views integration
 *
 * @author
 * Stefan Auditor <stefan.auditor@erdfisch.de>
 */

/**
 * Implementation of hook_views_data().
 */
function yatt_presence_views_data() {
  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['yatt_presence']['table']['group']  = t('Presence');

  $data['yatt_presence']['table']['join']['users'] = array(
    'left_field' => 'uid',
    'field' => 'uid',
    'type' => 'INNER',
  );
  
  $data['yatt_presence']['ypid'] = array(
    'title' => t('Yatt presence ID'),
    'help' => t('The yatt presence ID'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );

  $data['yatt_presence']['arrival'] = array(
    'title' => t('Arrival'), // The item it appears as on the UI,
    'help' => t('The date the user has arrived.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  $data['yatt_presence']['departure'] = array(
    'title' => t('Departure'), // The item it appears as on the UI,
    'help' => t('The date the user has departed.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  $data['yatt_presence']['location'] = array(
    'title' => t('Location'), // The item it appears as on the UI,
    'help' => t('The location the user was present.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  $data['yatt_presence']['created'] = array(
    'title' => t('Created'), // The item it appears as on the UI,
    'help' => t('The date the entry was created.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  $data['yatt_presence']['timediff'] = array(
    'title' => t('Time'),
    'help' => t('The time the user has been present.'),
    'field' => array(
        'handler' => 'yatt_presence_handler_field_timediff',
        'notafield' => TRUE,
    ),
  );
  return $data;
}

/**
 * Custom views handler definition
 */
function yatt_presence_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'yatt_presence') . '/includes',
    ),
    'handlers' => array(
      'yatt_presence_handler_field_timediff' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}
