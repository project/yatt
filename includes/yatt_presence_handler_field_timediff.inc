<?php

/**
 * @file
 * YATT Presence - Custom views handler
 *
 * @author
 * Stefan Auditor <stefan.auditor@erdfisch.de>
 */

/**
 * Timediff field handler.
 */
class yatt_presence_handler_field_timediff extends views_handler_field {

  function query() {
    $this->field_alias = 'yatt_presence_timediff_'. $this->position;
  }

  function render($values) {
    return format_interval($values->yatt_presence_departure - $values->yatt_presence_arrival);
  }
}
