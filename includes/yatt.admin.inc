<?php 

/**
 * @file
 * YATT - Yet Another Time Tracker - Administration page callbacks for YATT
 * 
 * @author
 * Marc Hitscherich <marc.hitscherich@erdfisch.de>
 * Stefan Auditor <stefan.auditor@erdfisch.de>
 */

/**
 * Form builder. Configure YATT
 */
function yatt_admin_settings() {
  $options = node_get_types('names');
  $size = count($options) < 15 ? count($options) : 15;

  $form['types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node type settings'),
    '#description' => t('Choose which node types should be handled as customers, projects and services.'),
  );
  $form['types']['yatt_customer_node_types'] = array(
    '#type' => 'select',
    '#title' => 'Customer node types',
    '#options' => $options,
    '#default_value' => variable_get('yatt_customer_node_types', array()),
    '#size' => $size,
    '#multiple' => TRUE,
    '#description' => t('Choose the customer node types.'),
  );
  
  $form['types']['yatt_project_node_types'] = array(
    '#type' => 'select',
    '#title' => 'Project node types',
    '#options' => $options,
    '#default_value' => variable_get('yatt_project_node_types', array()),
    '#size' => $size,
    '#multiple' => TRUE,
    '#description' => t('Choose the project node types.'),
  );
  
  $form['types']['yatt_service_node_types'] = array(
    '#type' => 'select',
    '#title' => 'Service node types',
    '#options' => $options,
    '#default_value' => variable_get('yatt_service_node_types', array()),
    '#size' => $size,
    '#multiple' => TRUE,
    '#description' => t('Choose the service node types.'),
  );

  $form['display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display settings'),
    '#description' => t('Choose default time input type.'),
  );
  $form['display']['yatt_default_display'] = array(
    '#type' => 'radios',
    '#title' => t('Select default input type for times'),
    '#options' => array(
        'textfield' => t('Textfield'),
        'selectables' => t('Selectables'),
    ),
    '#default_value' => variable_get('yatt_default_display', 'textfield'),
  );
  
  $form['selectables'] = array(
    '#type' => 'fieldset',
    '#title' => t('Selectables Settings'),
    '#description' => t('Set the range for selectables scala'),
  );
  $form['selectables']['yatt_selectables_timespan_from'] = array(
    '#type' => 'textfield',
    '#title' => t('Start value for the time scala'),
    '#default_value' => variable_get('yatt_selectables_timespan_from', '8'),
    '#size' => 5,
  );
  $form['selectables']['yatt_selectables_timespan_to'] = array(
    '#type' => 'textfield',
    '#title' => t('End value for the time scala'),
    '#default_value' => variable_get('yatt_selectables_timespan_to', '17'),
    '#size' => 5,
  );
  $form['estimation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Estimation'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['estimation']['yatt_estimation'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable time estimation'),
    '#default_value' => variable_get('yatt_estimation', TRUE),
  );
  $form['estimation']['yatt_estimation_percentage'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('yatt_estimation_percentage', 0),
    '#title' => t('Estimation buffer (in %)'),
  );
  
  return system_settings_form($form);
}
