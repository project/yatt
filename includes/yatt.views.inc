<?php

/**
 * @file
 * YATT - Yet Another Time Tracker - Un-/Install procedures
 *
 * @author
 * Marc Hitscherich <marc.hitscherich@erdfisch.de>
 * Stefan Auditor <stefan.auditor@erdfisch.de>
 */

/**
 *
 */
function yatt_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'yatt') . '/includes',
    ),
    'handlers' => array(
      'yatt_handler_field_timediff' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 *
 */
function yatt_views_data() {

  $data['yatt']['table']['group'] = t('YaTT');

  // YATT table
  $data['yatt']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'nid',
    'type' => 'INNER'
  );
  $data['yatt']['table']['join']['users'] = array(
    'left_field' => 'uid',
    'field' => 'uid',
    'type' => 'INNER'
  );
  $data['yatt']['table']['join']['comments'] = array(
    'left_field' => 'cid',
    'field' => 'cid',
    'type' => 'INNER'
  );

  // YATT ID field
  $data['yatt']['yid'] = array(
    'title' => t('YATT Id'),
    'help' => t('The YATT Id'),
    'field' => array(
        'handler' => 'views_handler_field',
    ),
  );

  // YATT nid
  $data['yatt']['nid'] = array(
    'title' => t('Service ID'),
    'help' => t('The Service ID'),
    'relationship' => array(
        'base' => 'node',
        'field' => 'nid',
        'handler' => 'views_handler_relationship',
        'label' => t('Service Id'),
    ),
  );

  // YATT uid
  $data['yatt']['uid'] = array(
    'title' => t('User'),
    'help' => t('The User'),
    'relationship' => array(
        'base' => 'users',
        'field' => 'uid',
        'handler' => 'views_handler_relationship',
        'label' => t('User'),
    ),
  );

  // YATT cid
  $data['yatt']['cid'] = array(
    'title' => t('Comment ID'),
    'help' => t('The Comment ID'),
    'relationship' => array(
        'base' => 'comment',
        'field' => 'cid',
        'handler' => 'views_handler_relationship',
        'label' => t('Comment Id'),
    ),
  );

  // YATT note
  $data['yatt']['note'] = array(
    'title' => t('Note'),
    'help' => t('a note'),
    'field' => array(
        'handler' => 'views_handler_field',
        'click_sortable' => FALSE,
    ),
  );

  // YATT start timestamp
  $data['yatt']['start'] = array(
    'title' => t('Start'),
    'help' => t('Start time'),
    'field' => array(
        'handler' => 'views_handler_field_date',
    ),
  );

  // YATT stop timestamp
  $data['yatt']['stop'] = array(
    'title' => t('Stop'),
    'help' => t('Stop time'),
    'field' => array(
        'handler' => 'views_handler_field_date',
    ),
    'filter' => array(
        'handler' => 'views_handler_filter_numeric'
    ),
  );

  // YATT created timestamp
  $data['yatt']['created'] = array(
    'title' => t('Created'),
    'help' => t('Created time'),
    'field' => array(
        'handler' => 'views_handler_field_date',
    ),
    'filter' => array(
        'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
        'handler' => 'views_handler_sort_date',
    ),
  );

  // YATT billable
  $data['yatt']['billable'] = array(
    'title' => t('Billable'),
    'help' => t('is this time-entry billable?'),
    'field' => array(
        'handler' => 'views_handler_field_boolean',
    ),
    'filter' => array(
        'handler' => 'views_handler_filter_boolean_operator',
    ),
  );

  $data['yatt']['timediff'] = array(
    'title' => t('Time'),
    'help' => t('Time / Duration'),
    'field' => array(
        'handler' => 'yatt_handler_field_timediff',
        'notafield' => TRUE,
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_pre_render().
 */
function yatt_views_pre_render(&$view) {
  if ($view->name == 'yatt_report_case') {
    return;
  }
  if ($view->name == 'yatt_report') {

    $is_paged = ($view->pager['use_pager'] && $view->pager['items_per_page'] < $view->total_rows);

    $diff_count = 0;
    foreach ($view->result as $row) {
      $diff_count += $row->yatt_stop - $row->yatt_start;
    }
    $time_diff = yatt_format_time_diff($diff_count, ':');


    if ($is_paged) {
      $diff_count_all = 0;
      $result = db_query($view->build_info['query'], $view->build_info['query_args']);
      while ($row = db_fetch_object($result)) {
        $diff_count_all += $row->yatt_stop - $row->yatt_start;
      }
      $time_diff_all = yatt_format_time_diff($diff_count_all, ':');
    }

    //TODO: remove inline styles
    $out = "<div id='yatt-report-count-wrapper' style='width:100%;border-top: 1px solid #ddd;'>";
    $out .= "<div id='yatt-report-count-left' style='float:left;'><strong>" . t('Sum: !time', array('!time' => $time_diff)) . "</strong></div>";
//    $out .= "<div id='yatt-report-count-right' style=';text-align:right;margin-right:178px;float:right;'>";
//    $out .= "<strong>{$time_diff}";
//    $out .= $is_paged ? "<br />({$time_diff_all})" : "";
//    $out .= "</strong></div>";
    $out .= "</div>";

    $view->attachment_after = $out;
  }

}
