<?php

/**
 * @file
 * YATT - Yet Another Time Tracker - Un-/Install procedures
 *
 * @author
 * Marc Hitscherich <marc.hitscherich@erdfisch.de>
 * Stefan Auditor <stefan.auditor@erdfisch.de>
 */

/**
 * Implementation of hook_views_default_views().
 */
function yatt_views_default_views() {

  $view = new view;
  $view->name = 'YaTT';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Standards', 'default');
  $handler->override_option('relationships', array(
  'uid' => array(
    'label' => 'Benutzer',
    'required' => 0,
    'id' => 'uid',
    'table' => 'yatt',
    'field' => 'uid',
    'relationship' => 'none',
  ),
  'nid' => array(
    'label' => 'Service Id',
    'required' => 0,
    'id' => 'nid',
    'table' => 'yatt',
    'field' => 'nid',
    'relationship' => 'none',
  ),
  ));
  $handler->override_option('fields', array(
  'yid' => array(
    'label' => 'yid',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'exclude' => 0,
    'id' => 'yid',
    'table' => 'yatt',
    'field' => 'yid',
    'relationship' => 'none',
  ),
  'title' => array(
    'label' => 'Case',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'link_to_node' => 1,
    'exclude' => 0,
    'id' => 'title',
    'table' => 'node',
    'field' => 'title',
    'relationship' => 'nid',
  ),
  'name' => array(
    'label' => 'User',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'link_to_user' => 1,
    'overwrite_anonymous' => 0,
    'anonymous_text' => '',
    'exclude' => 0,
    'id' => 'name',
    'table' => 'users',
    'field' => 'name',
    'relationship' => 'uid',
  ),
  'created' => array(
    'label' => 'Erstellt',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'date_format' => 'custom',
    'custom_date_format' => 'd.m.Y H:i',
    'exclude' => 0,
    'id' => 'created',
    'table' => 'yatt',
    'field' => 'created',
    'relationship' => 'none',
  ),
  'start' => array(
    'label' => 'Start',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'date_format' => 'custom',
    'custom_date_format' => 'H:i',
    'exclude' => 0,
    'id' => 'start',
    'table' => 'yatt',
    'field' => 'start',
    'relationship' => 'none',
  ),
  'stop' => array(
    'label' => 'Stop',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'date_format' => 'custom',
    'custom_date_format' => 'H:i',
    'exclude' => 0,
    'id' => 'stop',
    'table' => 'yatt',
    'field' => 'stop',
    'relationship' => 'none',
  ),
  'billable' => array(
    'label' => 'Billable',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'type' => 'yes-no',
    'not' => 0,
    'exclude' => 0,
    'id' => 'billable',
    'table' => 'yatt',
    'field' => 'billable',
    'relationship' => 'none',
  ),
  'note' => array(
    'label' => 'Note',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'strip_tags' => 0,
      'html' => 0,
  ),
    'exclude' => 0,
    'id' => 'note',
    'table' => 'yatt',
    'field' => 'note',
    'relationship' => 'none',
  ),
  ));
  $handler->override_option('sorts', array(
  'created' => array(
    'order' => 'ASC',
    'granularity' => 'minute',
    'id' => 'created',
    'table' => 'yatt',
    'field' => 'created',
    'relationship' => 'none',
  ),
  ));
  $handler->override_option('filters', array(
  'billable' => array(
    'operator' => '=',
    'value' => 'All',
    'group' => '0',
    'exposed' => TRUE,
    'expose' => array(
      'operator' => '',
      'identifier' => 'billable',
      'label' => 'Billable',
      'optional' => 1,
      'remember' => 0,
  ),
    'id' => 'billable',
    'table' => 'yatt',
    'field' => 'billable',
    'relationship' => 'none',
  ),
  ));
  $handler->override_option('access', array(
  'type' => 'none',
  ));
  $handler->override_option('cache', array(
  'type' => 'none',
  ));
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
  'grouping' => '',
  'override' => 1,
  'sticky' => 1,
  'order' => 'asc',
  'columns' => array(
    'yid' => 'yid',
    'title' => 'title',
    'name' => 'name',
    'created' => 'created',
    'start' => 'start',
    'stop' => 'stop',
    'billable' => 'billable',
    'note' => 'note',
  ),
  'info' => array(
    'yid' => array(
      'separator' => '',
  ),
    'title' => array(
      'sortable' => 0,
      'separator' => '',
  ),
    'name' => array(
      'sortable' => 0,
      'separator' => '',
  ),
    'created' => array(
      'separator' => '',
  ),
    'start' => array(
      'separator' => '',
  ),
    'stop' => array(
      'separator' => '',
  ),
    'billable' => array(
      'separator' => '',
  ),
    'note' => array(
      'separator' => '',
  ),
  ),
  'default' => '-1',
  ));
  $handler = $view->new_display('page', 'Seite', 'page_1');
  $handler->override_option('path', 'yattview');
  $handler->override_option('menu', array(
  'type' => 'none',
  'title' => '',
  'description' => '',
  'weight' => 0,
  'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
  'type' => 'none',
  'title' => '',
  'description' => '',
  'weight' => 0,
  ));
  $translatables['YaTT'] = array(
  t('Standards'),
  t('Page'),
  );
}
