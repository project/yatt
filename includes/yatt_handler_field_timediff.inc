<?php

/**
 * @file
 * YATT - Yet Another Time Tracker - Custom views handler
 * 
 * @author
 * Marc Hitscherich <marc.hitscherich@erdfisch.de>
 * Stefan Auditor <stefan.auditor@erdfisch.de>
 */

/**
 * Contains the 'timediff' field handler.
 */
class yatt_handler_field_timediff extends views_handler_field {
  
  function query() {
    $this->field_alias = 'yatt_timediff_'. $this->position;
  }
  
  function render($values) {
    return format_interval($values->yatt_stop - $values->yatt_start);
  }
}
