<?php

/**
 * @file
 * YATT Presence - Administration
 *
 * @author
 * Stefan Auditor <stefan.auditor@erdfisch.de>
 */

/**
 * Form builder. Configure YATT presence
 */
function yatt_presence_admin_settings() {
  $form['display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display settings'),
    '#description' => t('Choose where to display the presence toggle form. In addition you may use the presence toggle block activate it on the !url', array('!url' => l('Blocks page', 'admin/build/block'))),
  );
  $form['display']['yatt_presence_display_profile'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display presence toggle form on the profile page'),
    '#default_value' => variable_get('yatt_presence_display_profile', FALSE),
  );
  $form['display']['yatt_presence_display_node'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display presence toggle form on every node'),
    '#default_value' => variable_get('yatt_presence_display_node', FALSE),
  );
  $form['locations'] = array(
    '#type' => 'fieldset',
    '#title' => t('Location settings'),
    '#description' => t('You may provide some default locations for your users to choose from.'),
  );
  $form['locations']['yatt_presence_locations'] = array(
    '#type' => 'textarea',
    '#title' => t('Locations'),
    '#default_value' => variable_get('yatt_presence_locations', 'Office,Home Office'),
    '#description' => t('A comma seperated list of possible locations'),
  );
  return system_settings_form($form);
}
